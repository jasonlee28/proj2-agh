#include "utils.h"
#include <stdio.h>
#include <stdlib.h>

/* Function declaration for get_bits. */
int64_t get_bits(int64_t number, int a, int b);

/* Function declaration for sign_extend_double. */
int64_t sign_extend_double(int32_t word, int n);

/* Sign extends the given field to a 32-bit integer where field is
 * interpreted an n-bit integer. */
int sign_extend_number( unsigned int field, unsigned int n) {
  int32_t extended = (int32_t) field;
  return (extended << (32 - n)) >> (32 - n);
}

/* Sign extends the given number to a 64-bit integer. */
int64_t sign_extend_double(int32_t word, int n) {
  int64_t extended = (int64_t) word;
  return (extended << (64 - n)) >> (64 - n);
}

/* Unpacks the 32-bit machine code instruction given into the correct
 * type within the instruction struct */
Instruction parse_instruction(uint32_t instruction_bits) {
  unsigned int opcode = get_bits(instruction_bits, 0, 6);
  unsigned int rest = sign_extend_number((instruction_bits >> 7), 25);
  Instruction instruction;
  switch(opcode) {
    case 0x33:
      instruction.rtype.opcode = opcode;
      instruction.rtype.rd = (rest << 27) >> 27;
      instruction.rtype.funct3 = ((rest >> 5) << 24) >> 24;
      instruction.rtype.rs1 = ((rest >> 8) << 19) >> 19;
      instruction.rtype.rs2 = ((rest >> 13) << 14) >> 14;
      instruction.rtype.funct7 = ((rest >> 18) << 7) >> 7;
      break;
    case 0x13:
      instruction.itype.opcode = opcode;
      instruction.itype.rd = (rest << 27) >> 27;
      instruction.itype.funct3 = ((rest >> 5) << 24) >> 24;
      instruction.itype.rs1 = ((rest >> 8) << 19) >> 19;
      instruction.itype.imm = ((rest >> 13) << 7) >> 7;
      break;
    case 0x3:
      instruction.itype.opcode = opcode;
      instruction.itype.rd = (rest << 27) >> 27;
      instruction.itype.funct3 = ((rest >> 5) << 24) >> 24;
      instruction.itype.rs1 = ((rest >> 8) << 19) >> 19;
      instruction.itype.imm = ((rest >> 13) << 7) >> 7;
      break;
    case 0x23:
      instruction.stype.opcode = opcode;
      instruction.stype.imm5 = (rest << 27) >> 27;
      instruction.stype.funct3 = ((rest >> 5) << 24) >> 24;
      instruction.stype.rs1 = ((rest >> 8) << 19) >> 19;
      instruction.stype.rs2 = ((rest >> 13) << 14) >> 14;
      instruction.stype.imm7 = ((rest >> 18) << 7) >> 7;
      break;
    case 0x63:
      instruction.sbtype.opcode = opcode;
      instruction.sbtype.imm5 = (rest << 27) >> 27;
      instruction.sbtype.funct3 = ((rest >> 5) << 24) >> 24;
      instruction.sbtype.rs1 = ((rest >> 8) << 19) >> 19;
      instruction.sbtype.rs2 = ((rest >> 13) << 14) >> 14;
      instruction.sbtype.imm7 = ((rest >> 18) << 7) >> 7;
      break;
    case 0x37:
      instruction.utype.opcode = opcode;
      instruction.utype.rd = (rest << 27) >> 27;
      instruction.utype.imm = ((rest >> 5) << 7) >> 7;
      break;
    case 0x6F:
      instruction.ujtype.opcode = opcode;
      instruction.ujtype.rd = (rest << 27) >> 27;
      instruction.ujtype.imm = ((rest >> 5) << 7) >> 7;
      break;
    case 0x73:
      instruction.itype.opcode = opcode;
      instruction.itype.rd = (rest << 27) >> 27;
      instruction.itype.funct3 = ((rest >> 5) << 24) >> 24;
      instruction.itype.rs1 = ((rest >> 8) << 19) >> 19;
      instruction.itype.imm = ((rest >> 13) << 7) >> 7;
      break;
    default:
      break;

  }
  return instruction;
}

/* Return the number of bytes (from the current PC) to the branch label using the given
 * branch instruction */
int get_branch_offset(Instruction instruction) {
    int i5 = instruction.sbtype.imm5;
    int i7 = instruction.sbtype.imm7;
    int i4_1 = i5 >> 1;
    int pi4_1 = i4_1 << 1;
    int i10_5 = i7 << 1;
    int pi10_5 = i10_5 << 4;
    int i11 = (i5 >> 1) & 1;
    int pi11 = i11 << 10;
    int i12 = (i7 >> 6) & 1;
    int pi12 = i12 << 11;
    int offset = pi4_1 + pi10_5 + pi11 + pi12;
    return offset;
}

/* Returns the number of bytes (from the current PC) to the jump label using the given
 * jump instruction */
int get_jump_offset(Instruction instruction) {
    int i = instruction.ujtype.imm;
    int i10_1 = get_bits(i, 9, 18);
    int pi10_1 = i10_1 << 1;
    int i11 = get_bits(i, 8, 8);
    int pi11 = i11 << 11;
    int i19_12 = get_bits(i, 0, 7);
    int pi19_12 = i19_12 << 12;
    int i20 = get_bits(i, 19, 19);
    int pi20 = i20 << 20;
    int offset = pi10_1 + pi11 + pi19_12 + pi20;
    return offset;
}

int get_store_offset(Instruction instruction) {
    int i5 = instruction.stype.imm5;
    int i7 = instruction.stype.imm7;
    int pi7 = i7 << 5;
    int offset = i5 + pi7;
    return offset;
}

/* Returns bits a through b from number. */
int64_t get_bits(int64_t number, int a, int b) {
    int64_t result = 0;
    int bit;
    int count = 0;
    for (int i = a; i <= b; i += 1) {
      bit = (number >> i) & 1;
      result += bit << count;
      count += 1;
    }
    return result;
}

void handle_invalid_instruction(Instruction instruction) {
    printf("Invalid Instruction: 0x%08x\n", instruction.bits);
}

void handle_invalid_read(Address address) {
    printf("Bad Read. Address: 0x%08x\n", address);
    exit(-1);
}

void handle_invalid_write(Address address) {
    printf("Bad Write. Address: 0x%08x\n", address);
    exit(-1);
}
