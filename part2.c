#include <stdio.h> // for stderr
#include <stdlib.h> // for exit()
#include "types.h"
#include "utils.h"
#include "riscv.h"

void execute_rtype(Instruction, Processor *);
void execute_itype_except_load(Instruction, Processor *);
void execute_branch(Instruction, Processor *);
void execute_jal(Instruction, Processor *);
void execute_load(Instruction, Processor *, Byte *);
void execute_store(Instruction, Processor *, Byte *);
void execute_ecall(Processor *, Byte *);
void execute_lui(Instruction, Processor *);

void execute_instruction(uint32_t instruction_bits, Processor *processor,Byte *memory) {
    Instruction instruction = parse_instruction(instruction_bits);
    switch(instruction.opcode) {
        case 0x33:
            execute_rtype(instruction, processor);
            processor->PC += 4;
            break;
        case 0x13:
            execute_itype_except_load(instruction, processor);
            processor->PC += 4;
            break;
        case 0x73:
            execute_ecall(processor, memory);
            processor->PC += 4;
            break;
        case 0x63:
            execute_branch(instruction, processor);
            break;
        case 0x6F:
            execute_jal(instruction, processor);
            break;
        case 0x23:
            execute_store(instruction, processor, memory);
            processor->PC += 4;
            break;
        case 0x03:
            execute_load(instruction, processor, memory);
            processor->PC += 4;
            break;
        case 0x37:
            execute_lui(instruction, processor);
            processor->PC += 4;
            break;
        default: // undefined opcode
            handle_invalid_instruction(instruction);
            exit(-1);
            break;
    }
}

void execute_rtype(Instruction instruction, Processor *processor) {
    sDouble product = sign_extend_double(processor->R[instruction.rtype.rs1], 32) * sign_extend_double(processor->R[instruction.rtype.rs2], 32);
    switch (instruction.rtype.funct3){
        case 0x0:
            switch (instruction.rtype.funct7) {
                case 0x0:
                    // Add
                    processor->R[instruction.rtype.rd] = processor->R[instruction.rtype.rs1] + processor->R[instruction.rtype.rs2];
                    break;
                case 0x1:
                    // Mul
                    processor->R[instruction.rtype.rd] = get_bits(product, 0, 31);
                    break;
                case 0x20:
                    // Sub
                    processor->R[instruction.rtype.rd] = processor->R[instruction.rtype.rs1] - processor->R[instruction.rtype.rs2];
                    break;
                default:
                    handle_invalid_instruction(instruction);
                    exit(-1);
                    break;
            }
            break;
        case 0x1:
            switch (instruction.rtype.funct7) {
                case 0x0:
                    // SLL
                    processor->R[instruction.rtype.rd] = processor->R[instruction.rtype.rs1] << processor->R[instruction.rtype.rs2];
                    break;
                case 0x1:
                    // MULH
                    processor->R[instruction.rtype.rd] = (sWord) get_bits(product, 32, 63);
                    break;
            }
            break;
        case 0x2:
            // SLT
            if ((sWord) processor->R[instruction.rtype.rs1] < (sWord) processor->R[instruction.rtype.rs2]) {
              processor->R[instruction.rtype.rd] = 1;
            } else {
              processor->R[instruction.rtype.rd] = 0;
            }
            break;
        case 0x4:
            switch (instruction.rtype.funct7) {
                case 0x0:
                    // XOR
                    processor->R[instruction.rtype.rd] = processor->R[instruction.rtype.rs1] ^ processor->R[instruction.rtype.rs2];
                    break;
                case 0x1:
                    // DIV
                    processor->R[instruction.rtype.rd] = processor->R[instruction.rtype.rs1] / processor->R[instruction.rtype.rs2];
                    break;
                default:
                    handle_invalid_instruction(instruction);
                    exit(-1);
                    break;
            }
            break;
        case 0x5:
            switch (instruction.rtype.funct7) {
                case 0x0:
                    // SRL
                    processor->R[instruction.rtype.rd] = processor->R[instruction.rtype.rs1] >> processor->R[instruction.rtype.rs2];
                    break;
                case 0x20:
                    // SRA
                    processor->R[instruction.rtype.rd] = processor->R[instruction.rtype.rs1] >> processor->R[instruction.rtype.rs2];
                    break;
                default:
                    handle_invalid_instruction(instruction);
                    exit(-1);
                break;
            }
            break;
        case 0x6:
            switch (instruction.rtype.funct7) {
                case 0x0:
                    // OR
                    processor->R[instruction.rtype.rd] = processor->R[instruction.rtype.rs1] | processor->R[instruction.rtype.rs2];
                    break;
                case 0x1:
                    // REM
                    processor->R[instruction.rtype.rd] = processor->R[instruction.rtype.rs1] % processor->R[instruction.rtype.rs2];
                    break;
                default:
                    handle_invalid_instruction(instruction);
                    exit(-1);
                    break;
            }
            break;
        case 0x7:
            // AND
            processor->R[instruction.rtype.rd] = processor->R[instruction.rtype.rs1] & processor->R[instruction.rtype.rs2];
            break;
        default:
            handle_invalid_instruction(instruction);
            exit(-1);
            break;
    }
}

void execute_itype_except_load(Instruction instruction, Processor *processor) {
    int32_t imm = sign_extend_number(instruction.itype.imm, 12);
    switch (instruction.itype.funct3) {
        case 0x0:
            // ADDI
            processor->R[instruction.itype.rd] = processor->R[instruction.itype.rs1] + imm;
            break;
        case 0x1:
            // SLLI
            processor->R[instruction.itype.rd] = processor->R[instruction.itype.rs1] << imm;
            break;
        case 0x2:
            // STLI
            if ((int32_t) processor->R[instruction.itype.rs1] < imm) {
              processor->R[instruction.itype.rd] = 1;
            } else {
              processor->R[instruction.itype.rd] = 0;
            }
            break;
        case 0x4:
            // XORI
            processor->R[instruction.itype.rd] = processor->R[instruction.itype.rs1] ^ imm;
            break;
        case 0x5:
            // Shift Right (You must handle both logical and arithmetic)
            if (get_bits(processor->R[instruction.itype.rs1], 31, 31) == 1) {
              processor->R[instruction.itype.rd] = processor->R[instruction.itype.rs1] >> imm;
              int i;
              for (i = 0; i < imm; i += 1) {
                processor->R[instruction.itype.rd] ^= (1 << (31 - i));
              }
            } else {
              processor->R[instruction.itype.rd] = processor->R[instruction.itype.rs1] >> imm;
            }
            break;
        case 0x6:
            // ORI
            processor->R[instruction.itype.rd] = processor->R[instruction.itype.rs1] | imm;
            break;
        case 0x7:
            // ANDI
            processor->R[instruction.itype.rd] = processor->R[instruction.itype.rs1] & imm;
            break;
        default:
            handle_invalid_instruction(instruction);
            break;
    }
}

void execute_ecall(Processor *p, Byte *memory) {
    Register i;

    // syscall number is given by a0 (x10)
    // argument is given by a1
    switch(p->R[10]) {
        case 1: // print an integer
            printf("%d",p->R[11]);
            break;
        case 4: // print a string
            for(i=p->R[11];i<MEMORY_SPACE && load(memory,i,LENGTH_BYTE);i++) {
                printf("%c",load(memory,i,LENGTH_BYTE));
            }
            break;
        case 10: // exit
            printf("exiting the simulator\n");
            exit(0);
            break;
        case 11: // print a character
            printf("%c",p->R[11]);
            break;
        default: // undefined ecall
            printf("Illegal ecall number %d\n", p->R[10]);
            exit(-1);
            break;
    }
}

void execute_branch(Instruction instruction, Processor *processor) {
    switch (instruction.sbtype.funct3) {
        case 0x0:
            // BEQ
            if (processor->R[instruction.sbtype.rs1] == processor->R[instruction.sbtype.rs2]) {
              processor->PC += sign_extend_number(get_branch_offset(instruction), 12);
            } else {
              processor->PC += 4;
            }
            break;
        case 0x1:
            // BNE
            if (processor->R[instruction.sbtype.rs1] != processor->R[instruction.sbtype.rs2]) {
              processor->PC += sign_extend_number(get_branch_offset(instruction), 12);
            } else {
              processor->PC += 4;
            }
            break;
        default:
            handle_invalid_instruction(instruction);
            exit(-1);
            break;
    }
}

void execute_load(Instruction instruction, Processor *processor, Byte *memory) {
    Address addr = (Address) (processor->R[instruction.itype.rs1] + instruction.itype.imm);
    switch (instruction.itype.funct3) {
        case 0x0:
            // LB
            processor->R[instruction.itype.rd] = load(memory, addr, LENGTH_BYTE);
            break;
        case 0x1:
            // LH
            processor->R[instruction.itype.rd] = load(memory, addr, LENGTH_HALF_WORD);
            break;
        case 0x2:
            // LW
            processor->R[instruction.itype.rd] = load(memory, addr, LENGTH_WORD);
            break;
        default:
            handle_invalid_instruction(instruction);
            break;
    }
}

void execute_store(Instruction instruction, Processor *processor, Byte *memory) {
    int data = processor->R[instruction.stype.rs2];
    int offset = sign_extend_number(get_store_offset(instruction), 12);
    Address addr = (Address) (processor->R[instruction.stype.rs1] + offset);
    switch (instruction.stype.funct3) {
        case 0x0:
            // SB
            store(memory, addr, LENGTH_BYTE, (sByte) data);
            break;
        case 0x1:
            // SH
            store(memory, addr, LENGTH_HALF_WORD, (sHalf) data);
            break;
        case 0x2:
            // SW
            store(memory, addr, LENGTH_WORD, (sWord) data);
            break;
        default:
            handle_invalid_instruction(instruction);
            exit(-1);
            break;
    }
}

void execute_jal(Instruction instruction, Processor *processor) {
    Register counter = processor->PC;
    processor->R[instruction.ujtype.rd] = counter + 4;
    processor->PC += sign_extend_number(get_jump_offset(instruction), 20);
}

void execute_lui(Instruction instruction, Processor *processor) {
    processor->R[instruction.utype.rd] = (instruction.utype.imm << 12);
}

void store(Byte *memory, Address address, Alignment alignment, Word value) {
    int i;
    int count = 0;
    for (i = 0; i < alignment; i += 1) {
      memory[address + i] = get_bits(value, count, count + 7);
      count += 8;
    }
}

Word load(Byte *memory, Address address, Alignment alignment) {
    Word result = 0;
    int i;
    int count = 0;
    for (i = 0; i < alignment; i += 1) {
      result += memory[address + i] << count;
      count += 8;
    }
    int size = alignment * 8;
    return sign_extend_number(result, size);
}
